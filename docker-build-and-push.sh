#!/bin/sh

#Repository name created already on AWS ECR
REPOSITORY_NAME="awstestforspring"
REPOSITORY_URL=$(aws ecr describe-repositories \
                --repository-names ${REPOSITORY_NAME} \
                --profile tis_aws_profile  | jq .repositories[].repositoryUri | tr -d '"')

if [ -z $REPOSITORY_URL ]; then
    echo "ERROR: Repository could not found, Repository Url: " $REPOSITORY_URL
    exit 1
fi

# The tag that image will be build with
DOCKER_IMAGE_TAG=${bamboo.planRepository.branch}-${bamboo.buildNumber}

# Final docker image identity
DOCKER_IMAGE_IDENTITY=$REPOSITORY_NAME:$DOCKER_IMAGE_TAG
AWS_IMAGE_IDENTITY=$REPOSITORY_URL:$DOCKER_IMAGE_TAG

# Docker image build
docker image build -t $DOCKER_IMAGE_IDENTITY .

# Change name and tag of image build locally
docker tag $DOCKER_IMAGE_IDENTITY $AWS_IMAGE_IDENTITY

# Send final image to defined repository
docker push $AWS_IMAGE_IDENTITY