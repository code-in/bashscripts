  #!/bin/sh

  AWS_ACCES_KEY_ID_VALUE=xx
  AWS_SECRET_ACCES_KEY_VALUE=xx
  AWS_REGION=us-east-1

  AWS_ACCESS_KEY_ID=aws_access_key_id
  AWS_SECRET_ACCESS_KEY=aws_secret_access_key

  # Delegates to profile information for timeinstatus as in ./aws/credentials
  AWS_TIS_PROFILE_NAME=tis_aws_profile
  AWS_TIS_ACCESS_KEY_ID=$AWS_TIS_PROFILE_NAME.$AWS_ACCESS_KEY_ID
  AWS_TIS_SECRET_ACCESS_KEY=$AWS_TIS_PROFILE_NAME.$AWS_SECRET_ACCESS_KEY

  # Check abality of aws cli
  checkAwsExist() {
    isAwsExist=$(command -v aws)

    if [ -z $isAwsExist ]; then
      echo "-------------------------------------"
      echo "ERROR: Aws is currently not installed"
      exit 1
    else
      echo "Current aws version: "
      aws --version
    fi
  }

  # Set aws credentials authentication credentials by params
  configureAws() {
    awsAccessKeyId=$1
    awsSecretAccessKey=$2
    region=$3

    aws configure set $AWS_ACCESS_KEY_ID $awsAccessKeyId --profile $AWS_TIS_PROFILE_NAME
    aws configure set $AWS_SECRET_ACCESS_KEY $awsSecretAccessKey --profile $AWS_TIS_PROFILE_NAME
    aws configure set region $region --profile $AWS_TIS_PROFILE_NAME
  }

  # Checks aws credentials if or not exist
  checkAwsConfiguration() {
    echo "Checking aws configuration for profile name: " $AWS_TIS_PROFILE_NAME
    awsAccessKey=$(aws configure get $AWS_TIS_ACCESS_KEY_ID)
    awsSecretAccessKey=$(aws configure get $AWS_TIS_SECRET_ACCESS_KEY)
    
    if [ -z $awsAccessKey ] || [ -z $awsSecretAccessKey ]; then
      # If credentials are not exist, creta credentials with access id and key provided by aws console
      configureAws $AWS_ACCES_KEY_ID_VALUE $AWS_SECRET_ACCES_KEY_VALUE $AWS_REGION
    else
      echo "AWS already configured"
    fi
  }

  loginAwsEcr() {
    login_token=$(aws ecr get-login --profile $AWS_TIS_PROFILE_NAME --no-include-email)

    if [ -z "$login_token" ]; then
      echo "Could not take token"
      exit 1
    else
      if ! $login_token; then
        echo "AWS Login unsuccessful"
        exit 1
      fi
    fi
  }

  # If aws doesnt exist program will be broken
  checkAwsExist

  # Check credentials of aws configuration
  # checkAwsConfiguration
  
  # Set aws credentials
  configureAws $AWS_ACCES_KEY_ID_VALUE $AWS_SECRET_ACCES_KEY_VALUE $AWS_REGION

  # Login to aws docker repo
  loginAwsEcr
